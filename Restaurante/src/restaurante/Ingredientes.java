package restaurante;

/**
 *
 * @author neto_
 */
public class Ingredientes {

    private int porco;
    private int frango;
    private int lagosta;
    private int carneBovina;
    private int salada;

    public Ingredientes() {
        init();
    }

    public void init() {
        porco = 10;
        frango = 10;
        lagosta = 10;
        carneBovina = 10;
        salada = 10;
    }

    public int getPorco() {
        return porco;
    }

    public void setPorco(int aPorco) {
        porco = aPorco;
    }

    public int getFrango() {
        return frango;
    }

    public void setFrango(int aFrango) {
        frango = aFrango;
    }

    public int getLagosta() {
        return lagosta;
    }

    public void setLagosta(int aLagosta) {
        lagosta = aLagosta;
    }

    public int getCarneBovina() {
        return carneBovina;
    }

    public void setCarneBovina(int aCarneBovina) {
        carneBovina = aCarneBovina;
    }

    public int getSalada() {
        return salada;
    }

    public void setSalada(int aSalada) {
        salada = aSalada;
    }

    public void escolhePorco() {
        porco = porco - 1;
    }

    public void escolheFrango() {
        frango = frango - 1;
    }

    public void escolheCarneBovina() {
        carneBovina = carneBovina - 1;
    }

    public void escolheLagosta() {
        lagosta = lagosta - 1;
    }

    public void escolheSalada() {
        salada = salada - 1;
    }

    public String getStringPorco() {
        return "Porco";
    }

    public String getStringLagosta() {
        return "Lagosta";
    }

    public String getStringSalada() {
        return "Salada";
    }

    public String getStringCarne() {
        return "Carne";
    }

    public String getStringFrango() {
        return "Frango";
    }

}
