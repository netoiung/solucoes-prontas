package restaurante;

import java.util.Scanner;

/**
 *
 * @author Neto
 */
public class TextualUserInterface {

    public Prato prato;
    public Ingredientes ingredientes;

     Scanner entrada;
     int input;
        
    
    
    public TextualUserInterface() {
        prato = new Prato();
        ingredientes = new Ingredientes();
    }

    public void menuInicial() {
        System.out.println("#### Sistema Restaurante ####\n");
        System.out.println("#### 1 - Fazer Pedido ####\n");
        System.out.println("#### 2 - Visualizar Ingredientes ####\n");
        System.out.println("#### 0 - Sair ####\n");
    }
    
    public void menuEscolhePrato(){
        System.out.println("#### Escolher Prato ####\n");
        System.out.println("#### Digite o sabor do prato ####\n");
        System.out.println("#### Frango ####\n");
        System.out.println("#### Carne Bovina ####\n");
        System.out.println("#### Lagosta ####\n");
        System.out.println("#### Porco ####\n");
        System.out.println("#### Salada ####\n");
    }

    public Ingredientes getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(Ingredientes ingredientes) {
        this.ingredientes = ingredientes;
    }
    
    
    
    
   
    
    
}
