package restaurante;

/**
 *
 * @author neto_
 */
public class Prato {

    private Ingredientes ingredientes;
    
    public Prato() {
       ingredientes = new Ingredientes();
    }

    /**
     *
     * @param ingrediente
     */
    public void escolhePrato(String ingrediente) {
        if (ingrediente.equalsIgnoreCase("Porco")) {
            ingredientes.escolhePorco();
        } else if (ingrediente.equalsIgnoreCase("Frango")) {
            ingredientes.escolheFrango();
        } else if (ingrediente.equalsIgnoreCase("Lagosta")) {
            ingredientes.escolheLagosta();
        } else if (ingrediente.equalsIgnoreCase("Carne Bovina")) {
            ingredientes.escolheCarneBovina();
        } else if (ingrediente.equalsIgnoreCase("Salada")) {
            ingredientes.escolheSalada();
        }
    }

}
